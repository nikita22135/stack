#pragma once
#include <iostream>
template <typename T>
class Stack
{

public:
	explicit Stack(int size);

	T pop();

	void push(T element);

	~Stack();

private:
	T* arr;
	int size;
	int i;
};

