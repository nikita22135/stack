﻿#include <iostream>
#include "Stack.h"
#include "Stack.cpp"

int main()
{
    Stack<int> stack(2);
    stack.push(5);
    stack.push(3);
    std::cout << stack.pop()<<"\n";
    std::cout << stack.pop() << "\n";
    std::cout << stack.pop();
}

